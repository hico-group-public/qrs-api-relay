var config = {};

config.server = {};
config.certificates = {};
config.api = {};
config.app = {};

config.server.url = 'qliksense'; // Qlik Sense Server FQDN
config.server.port = 4242, // Port to QRS API

config.certificates.client_key = ".\\cert\\client_key.pem"; // Exported from the QMC
config.certificates.client_cert = ".\\cert\\client.pem"; // Exported from the QMC
config.certificates.https_key = ".\\cert\\server_key.pem"; // Valid SSL certificate/key
config.certificates.https_cert = ".\\cert\\server.pem"; // Valid SSL certificate/key

config.api.xrfkey = "abcdefghijklmnop"; // Random 16 character string

config.app.port = 8000; // HTTP port of the server
config.app.port_sec = 8443; // HTTPS port of the server

module.exports = config;