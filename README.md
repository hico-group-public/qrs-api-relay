NodeJS server that can be called from MENUBAR and will trigger API requests in Qlik Sense as different user.

### Config and setup

* clone/download the repo
* install NodeJS
* run `npm install`
* rename `config_example.js` to `config.js`
* edit `config.js` to match your environment
* paste the exported cerficates from qmc in the `\cert` folder.
* paste the SSL certificate and key into the `\cert` folder.
* in cmd type `node server.js`
* to install the server as a Windows service:
    * install `node-windows` globally on your machine using the cmds
        * `npm install -g node-windows`
        * and `npm link node-windows` in your project directoy

    * to install the server as Windows service type `node install_svc.js` in cmd
    * to uninstall Windows service type `node uninstall_svc.js` in cmd

### Usage

After the previous steps are completed and the server is running you can call the server using a MENUBAR button with a custom action using the script outlined in `button.js`