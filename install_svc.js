var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'HICO QRS API Relay',
  description: 'A NodeJS application, that can execute API commands as administrator.',
  script: require('path').join(__dirname,'server.js'), //'"C:\\Users\\Administrator\\Desktop\\node_tasks\\server.js"',
  nodeOptions: [
    '--harmony',
    '--max_old_space_size=4096'
  ]
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();

