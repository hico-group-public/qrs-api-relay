var config = require('./config');
var express = require('express');
var winston = require('winston'),
expressWinston = require('express-winston');

var fs = require('fs');
var http = require('http');
var https = require('https');


// Setup HTTPS certificate and key
var privateKey = fs.readFileSync(config.certificates.https_key, 'utf8');
var certificate = fs.readFileSync(config.certificates.https_cert, 'utf8');
var credentials = { key: privateKey, cert: certificate };

// Initialize Express-Server
var express = require('express');
var app = express();

// Configure CORS Whitelist
var allowCrossDomain = function (req, res, next) {
	// res.header('Access-Control-Allow-Origin', 'https://' + config.server.url);
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', '*');
	next();
}
app.use(allowCrossDomain);

// Setup logging
app.use(expressWinston.logger({
	transports: [
	  new winston.transports.Console({'timestamp':true})
	],
	format: winston.format.combine(
		winston.format.timestamp({
		  format: 'YYYY-MM-DD HH:mm:ss'
		}),
		winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`+(info.splat!==undefined?`${info.splat}`:" "))
	  ),
	metaField: null,
	meta: false,
	msg: "HTTP {{req.method}} {{req.url}}",
	expressFormat: true,
	colorize: true,
	ignoreRoute: function (req, res) { return false; }
  }));


// Basic request options for QRS requests
var options = {
	rejectUnauthorized: false,
	hostname: config.server.url,
	port: config.server.port,
	path: null,
	method: null,
	body: '',
	headers: {
		'X-Qlik-Xrfkey': config.api.xrfkey,
		'X-Qlik-User': 'UserDirectory=Internal; UserId=sa_repository'
	},
	key: fs.readFileSync(config.certificates.client_key),
	cert: fs.readFileSync(config.certificates.client_cert)
};


// Collect the correct task status messages from QRS
var taskResults = null;
(async function getEnums(){
	let path = '/qrs/about/api/enums' + '?xrfkey=' + config.api.xrfkey;
	options["path"] = path;
	options["method"] = 'GET';
	let rq = https.request(options, (qs_res) => {		
		var responseString = "";
		qs_res.on("data", (data) => {
			responseString += data;
		});
		qs_res.on("end", () => {
			// console.log(responseString);
			taskResults = JSON.parse(responseString).TaskExecutionStatus.values;
		});
		qs_res.on('error', error => {
			taskResults = [];
		});
	});
	rq.write('');
	rq.end();
})();


// Define allowed routes for the Express-Server

// GET: /
// Test to see if server is running.
app.get('/', function (req, res) {
	// console.log(taskResults);	
	res.send('Hello World!');
});


// GET: /start/task/:taskId
// Request the start of a task.
app.get('/start/task/:taskId', async function (req, res) {
	var path = '/qrs/task/' + encodeURIComponent(req.params.taskId) + '/start/synchronous' + '?xrfkey=' + config.api.xrfkey;
	// console.log('Task Start Request: ' + path);

	options["path"] = path;
	options["method"] = 'POST';

	let rq = https.request(options, (qs_res) => {
		var responseString = "";
		qs_res.on("data", (data) => {
			responseString += data;
		});
		qs_res.on("end", () => {
			res.status(qs_res.statusCode);
			res.send(JSON.parse(responseString));
		});
		qs_res.on('error', error => {
			res.status(qs_res.statusCode);
			res.send(error);
		});
	});
	rq.write('');
	rq.end();
});

// GET: /latestresult/task/:taskId
// Get the result of the last task execution
app.get('/latestresult/task/:taskId', async function (req, res) {
	var path = '/qrs/executionresult?xrfkey=' + config.api.xrfkey + '&filter=(TaskId%20eq%20' + encodeURIComponent(req.params.taskId) + ')';
	// console.log('Task Status Request: ' + path);

	options["path"] = path;
	options["method"] = 'GET';

	let rq = https.request(options, (qs_res) => {	
		var responseString = "";
		qs_res.on("data", (data) => {
			responseString += data;
		});
		qs_res.on("end", () => {
			res.status(qs_res.statusCode);
			res.send({
				"value": JSON.parse(responseString)[0].status,
				"status": taskResults[JSON.parse(responseString)[0].status]
					});
			
			
		});
		qs_res.on('error', error => {
			res.status(qs_res.statusCode);
			res.send(error);
		});
	});
	rq.write('');
	rq.end();
});


// GET: /status/execution/:handle
// Get the current status of a task execution identified by it's handle
// handle is being returned by /start/task/!
app.get('/status/execution/:handle', async function (req, res) {
	var path = '/qrs/executionsession/' + encodeURIComponent(req.params.handle) + '?xrfkey=' + config.api.xrfkey;
	// console.log('Execution Status Request: ' + path);

	options["path"] = path;
	options["method"] = 'GET';

	let rq = https.request(options, (qs_res) => {	
		var responseString = "";
		qs_res.on("data", (data) => {
			responseString += data;
		});
		qs_res.on("end", () => {
			if (qs_res.statusCode != 200) {
				res.status(200);
				res.send({
					"value": -1,
					"status": "-1: Execution not found."
						});
			} else {
				res.status(qs_res.statusCode);
				res.send({
						"value": JSON.parse(responseString).executionResult.status,
						"status": taskResults[JSON.parse(responseString).executionResult.status]
							});
			}
		});
		qs_res.on('error', error => {
			res.status(qs_res.statusCode);
			res.send(error);
		});
	});
	rq.write('');
	rq.end();
});

// Create Servers for HHTTP and HTTPS and start listening
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(8000);
httpsServer.listen(8443);
