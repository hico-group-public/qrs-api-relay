// Konfiguration
var taskId = "e33a0544-3ce1-470c-bec2-13b815aa7db4";
var textBoxId = "jPrntKp";

// Code
var buttonText = $(element).find("span");
var textbox = $("#" + textBoxId + "_content .qv-media-tool-html");
var origText = textbox.html();
var currentText = origText;
var handle = null;
var execRes = null;

// function setButtonText(newText){
//   if(newText == currentText){ return; }
//   console.log("Setting Button to: " + newText);
//   buttonText.html(newText);
//   currentText = newText;
// }

function setButtonText(newText){
  if(newText == currentText){ return; }
  console.log("Setting Button to: " + newText);
  textbox.html(newText);
  currentText = newText;
}

async function startTask(id){
  return $.ajax({
    url: "https://qliksense:8443/start/task/" + id,
    type: "GET",
    success: function (result){
      console.log("startTask OK");
      handle = result.value;
      setButtonText(origText + "<br/>Reload-Task: '1: Triggered'");
    },
    error: function (error){
      console.log("startTask ERROR:");
      console.log(error);
      setButtonText(origText + "<br/>FEHLER: Reload konnte nicht gestartet werden.");      
    }
  });
}

async function getExecStatus(handle){
    return $.ajax({
      url: "https://qliksense:8443/status/execution/" + handle,
      type: "GET",
      success: function (result){
        console.log("getExecStatus OK");
        if (execRes != -1){
            setButtonText(origText + "<br/>Reload-Status: '" + result.status + "'");
        }
        execRes = result.value;
      },
      error: function (error){
        console.log("getExecStatus ERROR:");
        console.log(error);
        setButtonText(origText + "<br/>FEHLER: Status nicht abrufbar.");      
      }
    });
  }

async function getTaskResult(id){
    return $.ajax({
      url: "https://qliksense:8443/latestresult/task/" + id,
      type: "GET",
      success: function (result){
        console.log("getTaskResult OK");
        execRes = result.value;
        setButtonText(origText + "<br/>Reload-Ergebnis: '" + result.status + "'");
      },
      error: function (error){
        console.log("getTaskResult ERROR:");
        console.log(error);
        setButtonText(origText + "<br/>FEHLER: Status nicht abrufbar.");      
      }
    });
  }

async function go(){
    await startTask(taskId);
    await new Promise(r => setTimeout(r, 1000));

    while (execRes != 7 && execRes != 8 && execRes != -1 && handle != "00000000-0000-0000-0000-000000000000") {
        await getExecStatus(handle);
        await new Promise(r => setTimeout(r, 1000));
    }

    await getTaskResult(taskId);
    await new Promise(r => setTimeout(r, 5000));
    setButtonText(origText);
}

go();
